import vue from 'rollup-plugin-vue'; // Handle .vue SFC files
import postcss from 'rollup-plugin-postcss';
import postcssNested from 'postcss-nested';
import { terser } from "rollup-plugin-terser";
import cssnano from "cssnano";

const postcssOptions = { 
                extract: true,
                plugins: [postcssNested, cssnano({preset: 'default'})] 
            }
export default [
    // UMD build to be used with webpack/rollup.
    {
        input: 'src/index.js',
        output: {
            name: 'MyComponent',
            format: 'umd',
            file: 'dist/library.umd.js'
        },
        plugins: [
            postcss(postcssOptions),
            vue(),
            terser()
        ]
    },
    // esm build.
    {
        input: 'src/index.js',
        output: {
            format: 'esm',
            file: 'dist/library.esm.js'
        },
        plugins: [
            postcss(postcssOptions),
            vue() //Terser breaks esm bundles
        ]
    },
    // Browser build.
    {
        input: 'src/wrapper.js',
        output: {
            format: 'iife',
            file: 'dist/library.js'
        },
        plugins: [
            postcss({ plugins: [postcssNested, cssnano({preset: 'default'})] }),
            vue(),
            terser()
        ]
    }
]
